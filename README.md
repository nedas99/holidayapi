# Holiday Api #

* ### Deployment instructions ###

Deployment was done with Visual Studio 2019. It creates projects resource group and app service plan. Database dependency configuration was needed, it asked to fill in the connection string for the app, which has to be:
**DBConnection**

* ### API documentation ###

API documentation can be found on:
https://holidayapi20211115150528.azurewebsites.net/swagger/index.html

* ### Deployment ###

API can be accessed through:
https://holidayapi20211115150528.azurewebsites.net/

Endpoint examples:
https://holidayapi20211115150528.azurewebsites.net/api/countries
https://holidayapi20211115150528.azurewebsites.net/api/holidays?year=2021&countryCode=ltu
https://holidayapi20211115150528.azurewebsites.net/api/status?date=2021-12-25&countryCode=ltu
https://holidayapi20211115150528.azurewebsites.net/api/freedays?year=2021&countryCode=ltu

* ### Testing ###

16 unit tests were done for the application.
Line coverage for controllers:
* CountriesController: 94.4%
* FreeDaysController: 100%
* HolidaysController: 93.9%
* StatusController: 91.6%

During development and publishment of the application, the endpoints have been called many times, if the endpoint is being called for the first time, it takes far longer to give a response than calling it after, because data is fetched from https://kayaposoft.com/enrico/ . Data is then placed in a database, so any call after the initial call is getting data from database, which is way faster.