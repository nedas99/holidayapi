﻿using HolidayApi.Data;
using System;
using System.Linq;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace HolidayApi.Tests
{
    public class TestBase : IDisposable
    {
        private SqliteConnection conn;

        protected DataContext context;

        protected TestBase()
        {
            conn = new SqliteConnection("DataSource=:memory:");
            conn.Open();

            var options = new DbContextOptionsBuilder<DataContext>()
                .UseSqlite(conn)
                .Options;

            context = new DataContext(options);
            context.Database.EnsureCreated();
        }

        public void ClearDb()
        {
            if (!context.Countries.Any())
            {
                context.RemoveRange(context.Countries);
            }
            if (!context.CountriesCheckedIntervals.Any())
            {
                context.RemoveRange(context.CountriesCheckedIntervals);
            }
            if (!context.CountriesDays.Any())
            {
                context.RemoveRange(context.CountriesDays);
            }
            context.SaveChanges();
        }

        public void Dispose()
        {
            conn.Close();
        }
    }
}
