﻿using HolidayApi.Controllers;
using HolidayApi.Data.Entities;
using HolidayApi.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using System.Linq;
using AutoMapper;
using HolidayApi.Data;

namespace HolidayApi.Tests
{
    public class CountriesTests : TestBase
    {
        private readonly CountriesController controller;
        private readonly ICountriesService countriesService;

        public CountriesTests()
        {
            var mapper = new MapperConfiguration(cfg => cfg.AddProfile(new AutoMapperProfiles())).CreateMapper();
            countriesService = new CountriesService(mapper);
            controller = new CountriesController(context, countriesService);
        }

        [Fact]
        public async Task GetCountriesTwice_ReturnsOkAndCountriesList()
        {
            // Arrange

            ClearDb();
            var countriesFromApi = await countriesService.GetCountriesFromApi();

            // Act

            // Calls external api first time, then gets from db:
            var response1 = await controller.GetCountries();
            var response2 = await controller.GetCountries();

            // Assert

            var result1 = Assert.IsType<OkObjectResult>(response1.Result);
            var countries1 = Assert.IsAssignableFrom<IEnumerable<Country>>(result1.Value);

            var result2 = Assert.IsType<OkObjectResult>(response2.Result);
            var countries2 = Assert.IsAssignableFrom<IEnumerable<Country>>(result2.Value);

            Assert.Equal(countriesFromApi.Count, countries1.Count());
            Assert.Equal(countriesFromApi.Count, countries2.Count());
        }
    }
}
