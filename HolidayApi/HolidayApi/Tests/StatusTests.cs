﻿using AutoMapper;
using HolidayApi.Controllers;
using HolidayApi.Data;
using HolidayApi.Data.Dtos;
using HolidayApi.Data.Entities;
using HolidayApi.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace HolidayApi.Tests
{
    public class StatusTests : TestBase
    {
        private readonly CountriesController countriesController;

        private readonly StatusController controller;

        public StatusTests()
        {
            var mapper = new MapperConfiguration(cfg => cfg.AddProfile(new AutoMapperProfiles())).CreateMapper();

            countriesController = new CountriesController(context, new CountriesService(mapper));
            var statusService = new StatusService(context);

            controller = new StatusController(context, statusService, mapper);
        }

        private async Task FillCountries()
        {
            await countriesController.GetCountries();
        }

        [Theory]
        [InlineData("2000-50-50", "ltu")]
        [InlineData("aaaa-aa-aa", "ltu")]
        [InlineData("1800-01-01", "ltu")]
        public async Task GetDaysStatus_ReturnsBadRequest(string date, string countryCode)
        {
            // Arrange

            await FillCountries();

            // Act

            var response = await controller.GetDaysStatus(date, countryCode);

            // Assert

            Assert.IsType<BadRequestObjectResult>(response.Result);
        }

        [Fact]
        public async Task GetDaysStatus_404CountryNotFound()
        {
            // Arrange

            await FillCountries();
            var date = "2020-01-01";
            var countryCode = "zzzz";

            // Act

            var response = await controller.GetDaysStatus(date, countryCode);

            // Assert

            Assert.IsType<NotFoundObjectResult>(response.Result);
        }

        [Theory]
        [InlineData("2021-12-25", "ltu", DayType.Holiday)]
        [InlineData("2021-11-11", "ltu", DayType.Workday)]
        [InlineData("2021-11-14", "ltu", DayType.FreeDay)]
        public async Task GetDaysStatus_ReturnsOkAndDayStatus(string date, string countryCode, DayType type)
        {
            // Arrange

            await FillCountries();

            // Act

            var response = await controller.GetDaysStatus(date, countryCode);

            // Assert

            var result = Assert.IsType<OkObjectResult>(response.Result);
            var statusResponse = Assert.IsType<StatusResponse>(result.Value);

            Assert.Equal(type.ToString(), statusResponse.Status);
        }
    }
}
