﻿using AutoMapper;
using HolidayApi.Controllers;
using HolidayApi.Data;
using HolidayApi.Data.Dtos;
using HolidayApi.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace HolidayApi.Tests
{
    public class FreedaysTests : TestBase
    {
        private readonly CountriesController countriesController;

        private readonly FreeDaysController controller;

        public FreedaysTests()
        {
            var mapper = new MapperConfiguration(cfg => cfg.AddProfile(new AutoMapperProfiles())).CreateMapper();

            countriesController = new CountriesController(context, new CountriesService(mapper));
            var statusService = new StatusService(context);
            var holidayService = new HolidayService(context, mapper);

            controller = new FreeDaysController(context, holidayService, statusService, mapper);
        }

        private async Task FillCountries()
        {
            await countriesController.GetCountries();
        }

        [Fact]
        public async Task GetMaxConsecutiveFreedays_CountryNotFound()
        {
            // Arrange

            await FillCountries();
            var year = 2020;
            var countryCode = "zzzz";

            // Act

            var response = await controller.GetMaxConsecutiveFreedays(year, countryCode);

            // Assert

            Assert.IsType<NotFoundObjectResult>(response.Result);
        }

        [Fact]
        public async Task GetMaxConsecutiveFreedays_400YearNotSupported()
        {
            // Arrange

            await FillCountries();
            var year = 1800;
            var countryCode = "ltu";

            // Act

            var response = await controller.GetMaxConsecutiveFreedays(year, countryCode);

            // Assert

            Assert.IsType<BadRequestObjectResult>(response.Result);
        }

        [Theory]
        [InlineData(2022, "ltu")]
        [InlineData(2020, "est")]
        [InlineData(2015, "lux")]
        public async Task GetMaxConsecutiveFreedays_ReturnsOk(int year, string countryCode)
        {
            // Arrange

            await FillCountries();

            // Act

            var response = await controller.GetMaxConsecutiveFreedays(year, countryCode);

            // Assert

            var result = Assert.IsType<OkObjectResult>(response.Result);
            var freedaysResponse = Assert.IsType<FreedaysResponse>(result.Value);

            // At the very least a regular weekend would be max consecutive freedays
            Assert.True(freedaysResponse.Count >= 2);
            Assert.True(freedaysResponse.Count == freedaysResponse.Days.Count);
            for (int i = 0; i < freedaysResponse.Count - 1; i++)
            {
                var firstDay = freedaysResponse.Days[i];
                var secondDay = freedaysResponse.Days[i + 1];
                Assert.True(secondDay.Date == firstDay.Date.AddDays(1));
            }
        }
    }
}
