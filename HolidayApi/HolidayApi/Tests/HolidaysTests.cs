﻿using AutoMapper;
using HolidayApi.Controllers;
using HolidayApi.Data;
using HolidayApi.Data.Dtos;
using HolidayApi.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace HolidayApi.Tests
{
    public class HolidaysTests : TestBase
    {
        private readonly HolidaysController controller;
        private readonly IHolidayService holidayService;
        private readonly CountriesController countriesController;

        public HolidaysTests()
        {
            var mapper = new MapperConfiguration(cfg => cfg.AddProfile(new AutoMapperProfiles())).CreateMapper();
            holidayService = new HolidayService(context, mapper);
            controller = new HolidaysController(mapper, context, holidayService);

            countriesController = new CountriesController(context, new CountriesService(mapper));
        }

        private async Task FillCountries()
        {
            await countriesController.GetCountries();
        }

        [Fact]
        public async Task GetCountriesHolidays_CountryNotFound()
        {
            // Arrange

            await FillCountries();
            var countryCode = "zzzz";

            // Act

            var response = await controller.GetCountriesHolidays(2020, countryCode);

            // Assert

            Assert.IsType<NotFoundObjectResult>(response.Result);
        }

        [Fact]
        public async Task GetCountriesHolidays_ReturnsBadRequestYearNotSupported()
        {
            // Arrange

            await FillCountries();
            var country = await context.Countries.FirstAsync();

            // Act

            var response = await controller.GetCountriesHolidays(country.FromDate.Year - 10, country.CountryCode);

            // Assert

            Assert.IsType<BadRequestObjectResult>(response.Result);
        }

        [Fact]
        public async Task GetCountriesHolidays_ReturnOkAndHolidays()
        {
            // Arrange

            await FillCountries();
            var countryCode = "ltu";
            var year = 2022;
            var holidaysFromApi = await holidayService.GetHolidaysFromApi(countryCode, year);

            // Act

            var response = await controller.GetCountriesHolidays(year, countryCode);

            // Assert

            var result = Assert.IsType<OkObjectResult>(response.Result);
            var holidayResponses = Assert.IsAssignableFrom<IEnumerable<MonthHolidaysResponse>>(result.Value);

            int holidayCount = holidayResponses.Sum(r => r.Holidays.Count);
            Assert.Equal(holidaysFromApi.Count, holidayCount);

            foreach (var holidayGroup in holidayResponses)
            {
                foreach (var holiday in holidayGroup.Holidays)
                {
                    Assert.Equal(holidayGroup.Month, holiday.Date.Month);
                    Assert.True(holidaysFromApi.Exists(h => h.Date.Equals(holiday.Date)));
                }
            }
        }
    }
}
