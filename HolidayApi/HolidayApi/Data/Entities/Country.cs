﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HolidayApi.Data.Entities
{
    public class Country
    {
        [Key]
        public string CountryCode { get; set; }

        public string Name { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}
