﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HolidayApi.Data.Entities
{
    public enum DayType
    {
        Workday,
        FreeDay,
        Holiday
    }

    public class CountriesDay
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public DayType Type { get; set; }
        public string HolidayName { get; set; }

        public string CountryId { get; set; }
        [ForeignKey("CountryId")]
        public Country Country { get; set; }
    }
}
