﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HolidayApi.Data.Entities
{
    public class CountriesCheckedInterval
    {
        public int Id { get; set; }
        public int YearChecked { get; set; }

        public string CountryId { get; set; }
        [ForeignKey("CountryId")]
        public Country Country { get; set; }
    }
}
