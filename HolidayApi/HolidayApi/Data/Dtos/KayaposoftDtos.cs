﻿using System.Collections.Generic;

namespace HolidayApi.Data.Dtos
{
    public class KayaposoftCountry
    {
        public string CountryCode { get; set; }
        public IEnumerable<string> Regions { get; set; }
        public IEnumerable<string> HolidayTypes { get; set; }
        public string FullName { get; set; }
        public KayaposoftDate FromDate { get; set; }
        public KayaposoftDate ToDate { get; set; }

        public KayaposoftCountry() { }
    }

    public class KayaposoftDate
    {
        public int Day { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }

        public KayaposoftDate() { }
    }

    public class KayaposoftDateOfWeek
    {
        public int Day { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public int DayOfWeek { get; set; }

        public KayaposoftDateOfWeek() { }
    }

    public class KayaposoftHoliday
    {
        public KayaposoftDateOfWeek Date { get; set; }
        public IEnumerable<KayaposoftHolidayName> Name { get; set; }
        public string HolidayType { get; set; }
    }

    public class KayaposoftHolidayName
    {
        public string Lang { get; set; }
        public string Text { get; set; }
    }
}
