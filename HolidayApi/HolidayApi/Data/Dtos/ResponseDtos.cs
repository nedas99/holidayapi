﻿using System;
using System.Collections.Generic;

namespace HolidayApi.Data.Dtos
{
    public class HolidaysResponse
    {
        public DateTime Date { get; set; }
        public string HolidayName { get; set; }
    }

    public class MonthHolidaysResponse
    {
        public int Month { get; set; }
        public List<HolidaysResponse> Holidays { get; set; }
    }

    public class StatusResponse
    {
        public DateTime Date { get; set; }
        public string Status { get; set; }
    }

    public class FreedaysResponse
    {
        public int Count { get; set; }
        public List<StatusResponse> Days { get; set; }
    }
}
