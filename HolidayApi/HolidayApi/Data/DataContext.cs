﻿using HolidayApi.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace HolidayApi.Data
{
    public class DataContext : DbContext
    {
        public DbSet<Country> Countries { get; set; }
        public DbSet<CountriesDay> CountriesDays { get; set; }
        public DbSet<CountriesCheckedInterval> CountriesCheckedIntervals { get; set; }

        public DataContext(DbContextOptions<DataContext> options) : base(options) { }
    }
}
