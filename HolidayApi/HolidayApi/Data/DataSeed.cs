﻿using HolidayApi.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using System.Threading.Tasks;

namespace HolidayApi.Data
{
    public static class DataSeed
    {
        public static async Task Seed(IServiceScope serviceScope)
        {
            var context = serviceScope.ServiceProvider.GetService<DataContext>();
            var countriesService = serviceScope.ServiceProvider.GetService<ICountriesService>();

            context.Database.Migrate();

            await FillCountries(context, countriesService);
        }

        public static async Task FillCountries(DataContext context, ICountriesService countriesService)
        {
            if (!context.Countries.Any())
            {
                var countries = await countriesService.GetCountriesFromApi();
                if (countries != null)
                {
                    context.Countries.AddRange(countries);
                    await context.SaveChangesAsync();
                }
            }
        }
    }
}
