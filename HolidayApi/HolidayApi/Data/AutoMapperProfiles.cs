﻿using AutoMapper;
using HolidayApi.Data.Dtos;
using HolidayApi.Data.Entities;
using System;
using System.Linq;

namespace HolidayApi.Data
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<KayaposoftCountry, Country>()
                .BeforeMap((src, dest) => src.FromDate.Year = src.FromDate.Year > DateTime.MaxValue.Year ? DateTime.MaxValue.Year : src.FromDate.Year)
                .BeforeMap((src, dest) => src.ToDate.Year = src.ToDate.Year > DateTime.MaxValue.Year ? DateTime.MaxValue.Year : src.ToDate.Year)
                .ForMember(dest => dest.Name,
                           opt => opt.MapFrom(src => src.FullName))
                .ForMember(dest => dest.FromDate,
                           opt => opt.MapFrom(src => new DateTime(src.FromDate.Year, src.FromDate.Month, src.FromDate.Day)))
                .ForMember(dest => dest.ToDate,
                           opt => opt.MapFrom(src => new DateTime(src.ToDate.Year, src.ToDate.Month, src.ToDate.Day)));


            CreateMap<KayaposoftHoliday, CountriesDay>()
                .BeforeMap((src, dest) => dest.Type = DayType.Holiday)
                .BeforeMap((src, dest) => src.Date.Year = src.Date.Year > DateTime.MaxValue.Year ? DateTime.MaxValue.Year : src.Date.Year)
                .ForMember(dest => dest.Date,
                           opt => opt.MapFrom(src => new DateTime(src.Date.Year, src.Date.Month, src.Date.Day)))
                /*.ForMember(dest => dest.HolidayName,
                           opt => opt.MapFrom(src => src.Name.First(n => n.Lang == "en").Text));*/
                .ForMember(dest => dest.HolidayName,
                           opt => opt.MapFrom(src => string.Join(',', src.Name.Select(n => $"{n.Lang}: {n.Text}"))));

            CreateMap<CountriesDay, HolidaysResponse>();

            CreateMap<CountriesDay, StatusResponse>()
                .ForMember(dest => dest.Status,
                           opt => opt.MapFrom(src => src.Type.ToString()));
        }
    }
}
