﻿using AutoMapper;
using HolidayApi.Data;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HolidayApi.Data.Entities;
using Microsoft.EntityFrameworkCore;
using HolidayApi.Data.Dtos;
using HolidayApi.Services;

namespace HolidayApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HolidaysController : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly DataContext context;
        private readonly IHolidayService holidayService;

        public HolidaysController(IMapper mapper, DataContext context, IHolidayService holidayService)
        {
            this.mapper = mapper;
            this.context = context;
            this.holidayService = holidayService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<MonthHolidaysResponse>>> GetCountriesHolidays(int year, string countryCode)
        {
            var country = await context.Countries.FirstOrDefaultAsync(c => c.CountryCode == countryCode);
            if (country == null)
            {
                return NotFound($"Country with code '{countryCode}' not found");
            }

            if (year < country.FromDate.Year || country.ToDate.Year < year)
            {
                return BadRequest($"Year {year} not supported for {country.Name}");
            }

            var yearChecked = await holidayService.IsYearChecked(year, countryCode);

            var holidays = await holidayService.GetCountriesHolidays(year, countryCode, yearChecked);

            if (holidays == null)
            {
                return NotFound("Problem with an external api");
            }

            var grouped = GroupHolidays(holidays);

            return Ok(grouped);
        }

        private IEnumerable<MonthHolidaysResponse> GroupHolidays(List<CountriesDay> holidays)
        {
            var mapped = mapper.Map<IEnumerable<HolidaysResponse>>(holidays);

            var grouped = mapped
                .GroupBy(h => h.Date.Month)
                .Select(g => new MonthHolidaysResponse
                {
                    Month = g.Key,
                    Holidays = g.ToList()
                });

            return grouped;
        }
    }
}
