﻿using HolidayApi.Data;
using HolidayApi.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HolidayApi.Data.Entities;
using AutoMapper;
using HolidayApi.Data.Dtos;
using System;

namespace HolidayApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FreeDaysController : ControllerBase
    {
        private readonly DataContext context;
        private readonly IHolidayService holidayService;
        private readonly IStatusService statusService;
        private readonly IMapper mapper;

        public FreeDaysController(DataContext context, IHolidayService holidayService, IStatusService statusService, IMapper mapper)
        {
            this.context = context;
            this.holidayService = holidayService;
            this.statusService = statusService;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<FreedaysResponse>> GetMaxConsecutiveFreedays(int year, string countryCode)
        {
            var country = await context.Countries.FirstOrDefaultAsync(c => c.CountryCode == countryCode);
            if (country == null)
            {
                return NotFound($"Country with code '{countryCode}' not found");
            }

            if (year < country.FromDate.Year || country.ToDate.Year < year)
            {
                return BadRequest($"Year {year} not supported for {country.Name}");
            }

            // Idea is that the maximum consecutive free days have to be around the holidays.
            // Every supported country has holidays.
            // Have to check each holiday and how many days before and after each holiday are not workdays.

            var yearChecked = await holidayService.IsYearChecked(year, countryCode);

            var holidays = await holidayService.GetCountriesHolidays(year, countryCode, yearChecked);

            // Saving the longest list of freedays:
            var freedays = new List<CountriesDay>();
            
            foreach (var holiday in holidays)
            {
                // If freedays has the current holiday,
                // then there's no need to check.
                // It might be the case where multiple holidays are consecutive,
                // running the algorithm already includes consecutive holidays.
                if (freedays.Exists(f => f.Date.Equals(holiday.Date)))
                {
                    continue;
                }

                var dayList = await FreeDaysAroundHoliday(holiday);
                if (dayList.Count > freedays.Count)
                {
                    freedays = dayList;
                }
            }

            var mapped = mapper.Map<List<StatusResponse>>(freedays);

            return Ok(new FreedaysResponse
            { 
                Count = mapped.Count,
                Days = mapped
            });
        }

        private async Task<List<CountriesDay>> FreeDaysAroundHoliday(CountriesDay holiday)
        {
            var freedays = new List<CountriesDay>();
            freedays.Add(holiday);

            // Searching for freedays going one day at a time to the past and to the future from the holiday:
            await HandleFreedaysAroundHoliday(freedays, holiday, -1);
            await HandleFreedaysAroundHoliday(freedays, holiday, 1);

            return freedays.OrderBy(f => f.Date).ToList();
        }

        private async Task HandleFreedaysAroundHoliday(List<CountriesDay> freedays, CountriesDay holiday, int incrementValue)
        {
            var date = holiday.Date;

            while (true)
            {
                date = date.AddDays(incrementValue);

                // Endpoint is for a specific year,
                // so if the day that's being checked is in another year
                // break the loop (might be the case around New Year's)
                if (date.Year != holiday.Date.Year)
                {
                    break;
                }

                // Get countries status, if it's a workday, break the loop
                var countriesDay = await statusService.GetCountriesDay(date, holiday.CountryId);
                if (countriesDay.Type == DayType.Workday)
                {
                    break;
                }
                freedays.Add(countriesDay);
            }
        }
    }
}
