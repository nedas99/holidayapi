﻿using HolidayApi.Data;
using HolidayApi.Data.Entities;
using HolidayApi.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HolidayApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountriesController : ControllerBase
    {
        private readonly DataContext context;
        private readonly ICountriesService countriesService;

        public CountriesController(DataContext context, ICountriesService countriesService)
        {
            this.context = context;
            this.countriesService = countriesService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Country>>> GetCountries()
        {
            // Database has no countries, calls the api, puts them in database and returns them:
            if (!context.Countries.Any())
            {
                var countriesFromApi = await countriesService.GetCountriesFromApi();
                if (countriesFromApi != null)
                {
                    context.Countries.AddRange(countriesFromApi);
                    await context.SaveChangesAsync();

                    return Ok(countriesFromApi);
                }
            }

            // Database has countries, we can return them directly:
            var countries = context.Countries;
            return Ok(countries);
        }
    }
}
