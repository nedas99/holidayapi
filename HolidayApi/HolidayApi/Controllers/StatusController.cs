﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using HolidayApi.Data;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using HolidayApi.Data.Dtos;
using HolidayApi.Services;

namespace HolidayApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StatusController : ControllerBase
    {
        private readonly DataContext context;
        private readonly IStatusService statusService;
        private readonly IMapper mapper;

        public StatusController(DataContext context, IStatusService statusService, IMapper mapper)
        {
            this.context = context;
            this.statusService = statusService;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<StatusResponse>> GetDaysStatus(
            [RegularExpression(@"([0-9])+-([0-9])+-([0-9])+", 
             ErrorMessage = "Date should be formatted YYYY-MM-DD")]string date, 
            string countryCode)
        {
            if (!DateTime.TryParse(date, out DateTime parsedDate))
            {
                return BadRequest("Incorrect date");
            }

            var country = await context.Countries.FirstOrDefaultAsync(c => c.CountryCode == countryCode);
            if (country == null)
            {
                return NotFound($"Country with code '{countryCode}' not found");
            }

            if (parsedDate.Year < country.FromDate.Year || country.ToDate.Year < parsedDate.Year)
            {
                return BadRequest($"Year {parsedDate.Year} not supported for {country.Name}");
            }

            var countriesDay = await statusService.GetCountriesDay(parsedDate, countryCode);

            if (countriesDay == null)
            {
                return NotFound("Problem with an external api");
            }

            var mapped = mapper.Map<StatusResponse>(countriesDay);

            return Ok(mapped);
        }
    }
}
