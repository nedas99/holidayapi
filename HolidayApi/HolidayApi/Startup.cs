using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using HolidayApi.Data;
using HolidayApi.Services;

namespace HolidayApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            string conn = Configuration.GetConnectionString("DBConnection");

            if (conn == null)
            {
                var server = Configuration["DBServer"] ?? "mssql";
                var port = Configuration["DBPort"] ?? "1433";
                var user = Configuration["DBUser"] ?? "SA";
                var password = Configuration["DBPassword"] ?? "";
                var database = Configuration["Database"] ?? "HolidayApiDb";

                conn = $"Server={server},{port};Initial Catalog={database};User ID={user};Password={password}";
            }

            services
                .AddDbContext<DataContext>(options =>
                {
                    options.UseSqlServer(conn);
                });

            services.AddControllers();
            services.AddAutoMapper(typeof(Startup));

            services.AddTransient<IHolidayService, HolidayService>();
            services.AddTransient<IStatusService, StatusService>();
            services.AddTransient<ICountriesService, CountriesService>();

            services.AddSwaggerDocument();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
