﻿using HolidayApi.Data;
using HolidayApi.Data.Entities;
using Microsoft.EntityFrameworkCore;
using RestSharp;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace HolidayApi.Services
{
    public interface IStatusService
    {
        Task<CountriesDay> GetCountriesDay(DateTime date, string countryCode);
    }

    public class StatusService : IStatusService
    {
        private readonly DataContext context;

        public StatusService(DataContext context)
        {
            this.context = context;
        }

        public async Task<CountriesDay> GetCountriesDay(DateTime date, string countryCode)
        {
            var day = await context.CountriesDays
                .Where(d => d.CountryId == countryCode)
                .Where(d => d.Date.Equals(date))
                .FirstOrDefaultAsync();

            // If the day was found in database, return it:
            if (day != null)
            {
                return day;
            }

            // Day was not found in database, need to call external api.

            // First check if the day is work day:
            var isWorkDay = await GetBoolValue("isWorkDay", date, countryCode);
            if (isWorkDay == null)
            {
                return null;
            }

            CountriesDay countriesDay;

            if ((bool)isWorkDay)
            {
                countriesDay = new CountriesDay
                {
                    CountryId = countryCode,
                    Date = date,
                    Type = DayType.Workday
                };
                context.CountriesDays.Add(countriesDay);
                await context.SaveChangesAsync();
                return countriesDay;
            }

            // If it's not a workday, it could be either holiday or a freeday:

            var isHoliday = await GetBoolValue("isPublicHoliday", date, countryCode);
            if (isHoliday == null)
            {
                return null;
            }

            DayType type = (bool)isHoliday ? DayType.Holiday : DayType.FreeDay;

            countriesDay = new CountriesDay
            {
                CountryId = countryCode,
                Date = date,
                Type = type
            };
            context.CountriesDays.Add(countriesDay);
            await context.SaveChangesAsync();
            return countriesDay;
        }

        private async Task<bool?> GetBoolValue(string action, DateTime date, string countryCode)
        {
            var client = new RestClient($"https://kayaposoft.com/enrico/json/v2.0/?action={action}&date={date.Day}-{date.Month}-{date.Year}&country={countryCode}");
            var request = new RestRequest(Method.GET);
            var response = await client.ExecuteAsync(request);
            if (!response.IsSuccessful)
            {
                return null;
            }

            // Two different types of calls to api are done,
            // both of them have one field, which has a boolean value,
            // so this check is made:
            return response.Content.Contains("true");
        }
    }
}
