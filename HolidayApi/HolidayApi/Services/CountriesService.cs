﻿using AutoMapper;
using HolidayApi.Data.Dtos;
using HolidayApi.Data.Entities;
using Newtonsoft.Json;
using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HolidayApi.Services
{
    public interface ICountriesService
    {
        Task<List<Country>> GetCountriesFromApi();
    }

    public class CountriesService : ICountriesService
    {
        private readonly IMapper mapper;

        public CountriesService(IMapper mapper)
        {
            this.mapper = mapper;
        }

        public async Task<List<Country>> GetCountriesFromApi()
        {
            var client = new RestClient("https://kayaposoft.com/enrico/json/v2.0/?action=getSupportedCountries");
            var request = new RestRequest(Method.GET);
            var response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                var content = JsonConvert.DeserializeObject<IEnumerable<KayaposoftCountry>>(response.Content);
                var mapped = mapper.Map<List<Country>>(content);

                return mapped;
            }
            return null;
        }
    }
}
