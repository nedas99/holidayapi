﻿using AutoMapper;
using HolidayApi.Data;
using HolidayApi.Data.Dtos;
using HolidayApi.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HolidayApi.Services
{
    public interface IHolidayService
    {
        Task<List<CountriesDay>> GetCountriesHolidays(int year, string countryCode, bool yearChecked);
        Task<List<CountriesDay>> GetHolidaysFromApi(string countryCode, int year);
        Task<bool> IsYearChecked(int year, string countryCode);
    }

    public class HolidayService : IHolidayService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;

        public HolidayService(DataContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<List<CountriesDay>> GetCountriesHolidays(int year, string countryCode, bool yearChecked)
        {
            List<CountriesDay> holidays;

            if (yearChecked)
            {
                // If the year is already checked in external api, get the data from database:
                holidays = await context.CountriesDays
                    .Where(d => d.CountryId == countryCode)
                    .Where(d => d.Date.Year == year)
                    .Where(d => d.Type == DayType.Holiday)
                    .OrderBy(d => d.Date)
                    .ToListAsync();

                return holidays;
            }

            // If the year was not called for the country, then
            // we need to call external api:
            holidays = await GetHolidaysFromApi(countryCode, year);


            // There could be duplicate holidays, 
            // if you first do /api/status, and then do this endpoint,
            // need to delete duplicates:
            var holidayDates = holidays
                .Select(h => h.Date);

            var existingHolidays = await context.CountriesDays
                .Where(d => holidayDates.Contains(d.Date))
                .Where(d => d.CountryId == countryCode)
                .ToListAsync();

            if (existingHolidays.Any())
            {
                context.CountriesDays.RemoveRange(existingHolidays);
            }

            // Add days and check the year, so next time
            // an external api wouldn't be called:
            context.CountriesDays.AddRange(holidays);
            context.CountriesCheckedIntervals.Add(new CountriesCheckedInterval
            {
                CountryId = countryCode,
                YearChecked = year
            });
            await context.SaveChangesAsync();

            return holidays;
        }

        public async Task<List<CountriesDay>> GetHolidaysFromApi(string countryCode, int year)
        {
            var client = new RestClient($"https://kayaposoft.com/enrico/json/v2.0/?action=getHolidaysForYear&year={year}&country={countryCode}");
            var request = new RestRequest(Method.GET);
            var response = await client.ExecuteAsync(request);

            if (!response.IsSuccessful)
            {
                return null;
            }

            // Converts external api data to one that can be put into the database.
            // Applies given country to those days:
            var content = JsonConvert.DeserializeObject<IEnumerable<KayaposoftHoliday>>(response.Content);
            var holidays = mapper.Map<List<CountriesDay>>(content);
            holidays.ForEach(d => d.CountryId = countryCode);
            holidays = holidays.OrderBy(h => h.Date).ToList();

            return holidays;
        }

        public async Task<bool> IsYearChecked(int year, string countryCode)
        {
            // Checks whether the year for the country is loaded from external api:
            var yearChecked = await context.CountriesCheckedIntervals
                .Where(i => i.CountryId == countryCode)
                .Where(i => i.YearChecked == year)
                .AnyAsync();

            return yearChecked;
        }
    }
}
