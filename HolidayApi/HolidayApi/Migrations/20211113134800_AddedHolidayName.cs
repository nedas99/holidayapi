﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HolidayApi.Migrations
{
    public partial class AddedHolidayName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "HolidayName",
                table: "CountriesDays",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HolidayName",
                table: "CountriesDays");
        }
    }
}
