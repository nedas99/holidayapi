﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HolidayApi.Migrations
{
    public partial class ReplacedYearInterval : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FromDate",
                table: "CountriesCheckedIntervals");

            migrationBuilder.DropColumn(
                name: "ToDate",
                table: "CountriesCheckedIntervals");

            migrationBuilder.AddColumn<int>(
                name: "YearChecked",
                table: "CountriesCheckedIntervals",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "YearChecked",
                table: "CountriesCheckedIntervals");

            migrationBuilder.AddColumn<DateTime>(
                name: "FromDate",
                table: "CountriesCheckedIntervals",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "ToDate",
                table: "CountriesCheckedIntervals",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
